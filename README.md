# dodgebot

Let's build an application to simplify dodgeball tournament organization.

## Goals

### Non technical

* Show game scores.
* Show game schedules.
* Used in round-robin phase.
* Used in elimination phase.
* Can handle women, men and co-head in the same tournement.
* Used by players to know when and where the play.
* Used by referees to update the scores.
* Used by referees to know their schedules.
* Used by organizers to update score.
* Used by organizers to send informations.
* In a case of a total failure, the organizers need
  to be able continue with the usual sheet and paper
  technique.
* No login required by players.
* Login required by organizers and referees.



### Technical

* A read failure should be only possible by a failure
  of the cloud provider (GCP or AWS).
* Website should be entirely static and read access
  should be available as long as cloud provider is up.
* Should have a public and well defined public API.
* Should have a google spreadsheet client.


## No Goals

* Supports other sports than dodgeball
* Supports multiple tournements (but this migh change)

## Questions asked

During the precedent quebec tournament, we sat next to the
organizers and we have noted the questions people asked them.
We believe all of these questions should be trivial to answers
with *dodgebot*.

|  Who asked | Question | Notes |
|:-----------|:----------|:-----|
| organizer  | How many games left in the tournament? |  |
| player     | When do I play next? | The answer could be in many categories |
| organizer  | Which courts are currently used? | |
| player     | At what time do I start the day? | |
| organizer  | Who plays in team X? | |
| referee    | Do you need me to referee any games soon? | |
| player     | In which rank did we ended up in?       | |
| organizer  | Who currently plays on court X?         |  |
| organizer  | Where were playing team X against Y?    |  |
| organizer  | Are the teams that plays on court X plays immediatly after? | Useful to handle lateness contagion. |

